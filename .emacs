(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; BACKUP DIRECTORY
(setq backup-directory-alist '(("." . "~/.saves")))

;; TAB SIZE
;;(setq-default tab-width 4)

;; DISABLE BARS
(tool-bar-mode 0)
(scroll-bar-mode 0)
(menu-bar-mode 0)

;; SPLASH SCREEN
(setq inhibit-startup-message t)

;; Compile
(global-set-key (kbd "M-£") 'compile)

;; CALL TERM
(global-set-key (kbd "M-<return>") 'term)

;; STRING RECTANGLE
(global-set-key (kbd "M-]") 'string-rectangle)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(spolsky))
 '(custom-safe-themes
   '("58c6711a3b568437bab07a30385d34aacf64156cc5137ea20e799984f4227265" "9b59e147dbbde5e638ea1cde5ec0a358d5f269d27bd2b893a0947c4a867e14c1" "c48551a5fb7b9fc019bf3f61ebf14cf7c9cdca79bcb2a4219195371c02268f11" default))
 '(package-selected-packages
   '(multiple-cursors company company-c-headers go go-mode lsp-mode move-text powerline sublime-themes yasnippet yasnippet-snippets)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil)))))

;; POWERLINE
(powerline-default-theme)

;; MULTIPLE CURSORS
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)

;; MOVE LINE
(global-set-key (kbd "M-p") 'move-text-line-up)
(global-set-key (kbd "M-n") 'move-text-line-down)

;; COMPANY
(add-hook 'after-init-hook 'global-company-mode)

;; YASNIPPET
(require 'yasnippet)
(yas-global-mode 1)

;; CCPP
(require 'cc-mode)
(require 'company)
(setq company-clang-executable "/usr/bin/clang")

